<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('users')->insert(
               [
                   [
                       'name' => 'jack',
                       'email' => 'jack@gmail.com',
                       'password' =>'12345678',
                       'created_at' => date('Y-m-d G:i:s'),
                   ],
                   [
                    'name' => 'roni',
                    'email' => 'roni@gmail.com',
                    'password' =>'12345678',
                    'created_at' => date('Y-m-d G:i:s'),
                   ],
                   [
                    'name' => 'booba',
                    'email' => 'booba@jack.com',
                    'password' =>'12345678',
                    'created_at' => date('Y-m-d G:i:s'),
                   ],
               ]);
           }
    }
       