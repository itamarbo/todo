<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return "Hello World!!";
});

Route::get('/student/{id?}', function ($id = 'no student provided') {
    return "Hello Student ".$id;
})->name('students'); //  -> החץ פותח אופציות לכל ראוט, למשל כאן הגדרנו את שם הראוט בשם student

Route::get('/comment/{id}', function ($id) {
    return view('comment',['id' => $id]);
})->name('comments');

Route::resource('todos', 'TodoController')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
